// pages/index.js
import Head from 'next/head';
import Dice from '../components/Dice';
import { useState } from 'react';
import Namtao from '@/components/Namtao';

export default function Home() {
  const [Choosemenu, setChoosemenu] = useState("ไฮโล")
  return (
    <div className='bg-black h-full'>
      <Head>
        <title>Pussy Game</title>
        <meta name="description" content="Roll a dice with Next.js and CSS Animation" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <div className='flex w-full text-white border-white '>
          <div onClick={()=>setChoosemenu("ไฮโล")}  className={` text-center font-bold text-4xl w-1/2 py-5 border-r ${Choosemenu=="ไฮโล"?" bg-pink-400 ":" bg-gray-400 "}`}>สูง-ต่ำ</div>
          <div onClick={()=>setChoosemenu("น้ำเต้าปูปลา")} className={` text-center font-bold text-4xl w-1/2 py-5 ${Choosemenu=="น้ำเต้าปูปลา"?" bg-pink-400 ":" bg-gray-400 "}`}>น้ำเต้า</div>
        </div>
        {Choosemenu=="ไฮโล"?
        <Dice/>
        :Choosemenu=="น้ำเต้าปูปลา"?
        <Namtao/>:""}
      </main>
    </div>
  );
}
