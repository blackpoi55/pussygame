import React, { useState, useEffect } from 'react';
// import diceSound from './sound/Dice.mp3'; // ปรับเปลี่ยนเส้นทางไปยังไฟล์เสียงตามที่คุณต้องการ

const Namtao = () => {
  const diceSound = '/sound/Dice.mp3';
  const [results, setResults] = useState([1, 1, 1]);
  const [rolling, setRolling] = useState(false);
  const [opened, setOpened] = useState([false, false, false]);
  const [AutoOpen, setAutoOpen] = useState(false);
  const rollDiceSound = new Audio(diceSound); // สร้างอ็อบเจ็กต์เสียง

  const rollDice = () => {
    if (rolling) return;
    setRolling(true);
    rollDiceSound.currentTime = 0; // รีเซ็ตเสียงให้เริ่มที่ต้น
    rollDiceSound.play(); // เล่นเสียงทันทีเมื่อทอยลูกเต๋า

    const rolls = [1, 2, 3];
    const newResults = Array.from({ length: 3 }, () => Math.floor(Math.random() * 6) + 1);

    while (newResults.length !== rolls.length) {
      const roll = Math.floor(Math.random() * 6) + 1;
      if (!newResults.includes(roll)) {
        newResults.push(roll);
      }
    }

    const timeoutId = setTimeout(() => {
      setResults(newResults);
      setRolling(false);
      if (AutoOpen) {
        setOpened([true, true, true]);
      }
      else {
        setOpened([false, false, false]);
      }
    }, 2000);

    return () => clearTimeout(timeoutId);
  };

  const toggleDice = (index) => {
    if (!rolling) {
      const newOpened = [...opened];
      newOpened[index] = true;
      setOpened(newOpened);
    }
  };

  const sumValueAll = (data, index) => {
    if (index === 0) {
      return data === 1 ? 'น้ำเต้า' : data === 2 ? 'ปู' : data === 3 ? 'ปลา' : data === 4 ? 'เสือ' : data === 5 ? 'ไก่' : data === 6 ? 'กุ้ง' : '';
    } else {
      return data === 1 ? ' , น้ำเต้า' : data === 2 ? ' , ปู' : data === 3 ? ' , ปลา' : data === 4 ? ' , เสือ' : data === 5 ? ' , ไก่' : data === 6 ? ' , กุ้ง' : '';
    }
  };

  return (
    <div className="text-center my-10 text-white">
      <div className='w-full flex justify-end'>
        {AutoOpen
          ? <button onClick={() => setAutoOpen(false)} className='mr-5 bg-green-400 p-2 rounded-lg font-bold'>เปิดอัตโนมัติ</button>
          : <button onClick={() => setAutoOpen(true)} className='mr-5 bg-blue-400 p-2 rounded-lg font-bold'>เปิดเอง</button>}
      </div>
      <h1 className="text-3xl mb-4 font-bold">น้ำเต้าปูปลา</h1>
      <div className={`flex justify-center space-x-4 ${rolling ? 'animate-shake' : ''}`}>
        {results.map((result, index) => (
          <div
            key={index}
            className={`w-24 h-24 bg-black border border-gray-400 rounded-full flex items-center justify-center text-3xl font-semibold cursor-pointer text-white ${opened[index] ? 'bg-pink-500 ' : ''
              }`}
            onClick={() => toggleDice(index)}
          >
            {opened[index] ? <img src={`/images/${result}.png`} alt="Dice" /> : '?'}
          </div>
        ))}
      </div>
      <button
        className={`mt-6 px-8 py-4 bg-blue-500 text-white rounded-full hover:bg-blue-600 disabled:bg-gray-300 ${rolling ? 'animate-shake' : ''
          }`}
        onClick={rollDice}
        disabled={rolling}
      >
        ทอยลูกเต๋า
      </button>
      {(opened[0] && opened[1] && opened[2]) && (
        <div className="mt-8">
          <p className={`text-5xl font-semibold 'text-blue-500'`}>
            {results.map((result, index) => (
              <span key={index}>
                {sumValueAll(result, index)}
              </span>
            ))}
          </p>
        </div>
      )}
      <audio id="diceAudio" src={diceSound}></audio>
    </div>
  );
};

export default Namtao;
