import React, { useState, useEffect } from 'react';

const Dice = () => {
  const [results, setResults] = useState([1, 1, 1]);
  const [rolling, setRolling] = useState(false);
  const [opened, setOpened] = useState([false, false, false]);
  const [totalResult, setTotalResult] = useState(null);
  const [AutoOpen, setAutoOpen] = useState(false);

  const diceSound = '/sound/Dice.mp3';

  useEffect(() => {
    let rollDiceSound; // Declare the variable here

    if (rolling) {
      const rolls = [1, 2, 3];
      const newResults = Array.from({ length: 3 }, () => Math.floor(Math.random() * 6) + 1);

      while (newResults.length !== rolls.length) {
        const roll = Math.floor(Math.random() * 6) + 1;
        if (!newResults.includes(roll)) {
          newResults.push(roll);
        }
      }

      const total = newResults.reduce((acc, val) => acc + val, 0);
      let resultText = '';

      if (total <= 10) {
        resultText = 'ต่ำ';
      } else if (total === 11) {
        resultText = 'ไฮโล';
      } else {
        resultText = 'สูง';
      }

      const timeoutId = setTimeout(() => {
        setResults(newResults);
        setRolling(false);
        if (AutoOpen) {
          setOpened([true, true, true]);
        }
        else{
          setOpened([false, false, false]);
        }
        setTotalResult({ total, resultText });
      }, 3000);

      // Create the Audio object only on the client-side
      rollDiceSound = new Audio(diceSound);

      // Play the sound immediately when rolling the dice
      rollDiceSound.currentTime = 0; // Reset sound to start
      rollDiceSound.play();

      return () => {
        clearTimeout(timeoutId);
        // Stop playing the sound when the component is unmounted
        if (rollDiceSound) {
          rollDiceSound.pause();
          rollDiceSound.currentTime = 0;
        }
      };
    }
  }, [rolling, diceSound]);

  const rollDice = () => {
    if (rolling) return;
    setRolling(true);
  };

  const toggleDice = (index) => {
    if (!rolling) {
      const newOpened = [...opened];
      newOpened[index] = true;
      setOpened(newOpened);
    }
  };

  return (
    <div className="text-center my-10 text-white">
      <div className='w-full flex justify-end'>
        {AutoOpen
          ? <button onClick={() => setAutoOpen(false)} className='mr-5 bg-green-400 p-2 rounded-lg font-bold'>เปิดอัตโนมัติ</button>
          : <button onClick={() => setAutoOpen(true)} className='mr-5 bg-blue-400 p-2 rounded-lg font-bold'>เปิดเอง</button>}
      </div>
      <h1 className="text-3xl mb-4 font-bold">สูง-ต่ำ</h1>
      <div className={`flex justify-center space-x-4 ${rolling ? 'animate-shake' : ''}`}>
        {results.map((result, index) => (
          <div
            key={index}
            className={`w-24 h-24 bg-black border border-gray-400 rounded-full flex items-center justify-center text-3xl font-semibold cursor-pointer text-white ${opened[index] ? 'bg-pink-500 ' : ''
              }`}
            onClick={() => toggleDice(index)}
          >
            {opened[index] ? result : '?'}
          </div>
        ))}
      </div>
      <button
        className={`mt-6 px-8 py-4 bg-blue-500 text-white rounded-full hover:bg-blue-600 disabled:bg-gray-300 ${rolling ? 'animate-shake' : ''
          }`}
        onClick={rollDice}
        disabled={rolling}
      >
        ทอยลูกเต๋า
      </button>
      {totalResult && opened.every(value => value === true) && (
        <div className="mt-8">
          <p className={`text-5xl font-semibold ${totalResult.resultText === 'ต่ำ' ? 'text-red-500' :
            totalResult.resultText === 'สูง' ? 'text-green-500' :
              'text-blue-500'
            }`}>
            {totalResult.total} ({totalResult.resultText})
          </p>
        </div>
      )}
    </div>
  );
};

export default Dice;
